import api from "../utils/api"


export const GET_ALL_USER = async () => {
    const response = await api.get("/users")
    return response.data;
}

export const CREATE_USER = async(newUser)=>{
    const response = await api.post("/users",newUser)
    return response.data
}

export const GET_ONE_USER = async(id)=>{
    const response = await api.get(`/users/${id}`)
    return response.data
}

