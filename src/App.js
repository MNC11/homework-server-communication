import './App.css';
import MyNavbar from './components/MyNavbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserCard from './components/UserCard';
import { BrowserRouter as BigRouter , Routes,Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import { AllUsers } from './pages/AllUsers';
import { NewUser } from './pages/NewUser';
import 'react-toastify/dist/ReactToastify.css';
import ViewProfile from './pages/UserDetail';
// import UserDetail from './pages/UserDetail';


function App() {
  return (
    <div className="App">
      <BigRouter>
        <MyNavbar/>
        <Routes>
          <Route path="" index element={<HomePage/>}/>
          <Route path="AllUser" element={<AllUsers />} />
          <Route path="AllUser/:id" element={<ViewProfile/>}/>
          <Route path="NewUser" element={<NewUser/>}/>
        </Routes>
      </BigRouter>
      {/* <MyNavbar/> */}
      {/* <h1>Hello React Js</h1> */}
      {/* <UserCard/> */}
    </div>
  );
}

export default App;
