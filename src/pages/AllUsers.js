
import React, { useEffect, useState } from 'react'
import UserCard from '../components/UserCard'
import { GET_ALL_USER } from '../service/userService'
import { ThreeDots } from  'react-loader-spinner'

export const AllUsers = () => {
    
    const [users, setUsers] = useState([])
    const [isloading, setIslaoding] = useState(true)
    const [keyword, setKeyword] = useState("")
    const [filteredUser , setFilteredUser] = useState([])
    
    let filterUserList = null;
    useEffect(() => {
        GET_ALL_USER().
            then((respone) => {
                setUsers(respone);
                setIslaoding(false);
                setFilteredUser(respone)
            }).
            catch((error) => console.log("Error :", error));
    }, []);

    useEffect(() => {
        if (keyword === null || keyword.trim() === "") {
            filterUserList = users;
        }
        else {
            filterUserList = users.filter((user) =>
                (user.name.toLowerCase().startsWith(keyword.toLowerCase())) ||
                (user.email.toLowerCase().startsWith(keyword.toLowerCase())) ||
                (user.role.toLowerCase().startsWith(keyword.toLowerCase()))
            )
        }
        setFilteredUser(filterUserList)
    }, [keyword])
    
    console.log("here are all teh user : ",users.sort((usera, userb) =>
    usera.id > userb.id ? -1 : usera.id < userb.id ? 1 : 0))
    
    return (
        <div className="container">
            {
                isloading ? <div className="container d-flex justify-content-center align-items-center vh-100">
                    <ThreeDots 
                        height="100" 
                        width="100" 
                        radius="10"
                        color="orange"
                        ariaLabel="three-dots-loading"
                        wrapperStyle={{}}
                        wrapperClassName=""
                        visible={true}
                    />
                </div>
                :
                <div className="d-flex flex-column">
                    <div className="custom d-flex  justify-content-between m-4">
                        <h2>All User : <span className='text-warning'>{filteredUser.length}</span></h2>
                            
                        <input type="text" className='form-control w-25 m-2 border border-warning' placeholder='Search By name email role ...'
                                onChange={(e) => {
                                    setKeyword(e.target.value)
                                console.log("Value that input from keyboard :" , e.target.value)
                            }}
                        />
                            
                    </div>     
                    <div className="row gy-4 ">
                        {filteredUser.length === 0 ? (
                            <div>
                            <h3 className="text-center"> No result </h3>
                            </div>
                        ) : (
                            <></>
                        )}

                        {filteredUser.map((user) => (
                            <div className="col-4 d-flex justify-content-center">
                            <UserCard user={user} />
                            </div>
                        ))}
                            
                    </div>

                </div>
            }
                        
        </div>
    )
}
