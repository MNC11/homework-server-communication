import React, { useState } from 'react'
import { ThreeDots } from 'react-loader-spinner';
import { toast, ToastContainer } from 'react-toastify';
import { CREATE_USER } from '../service/userService';

export const NewUser = () => {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("");
    const [avatar, setAvatar] = useState("");
    const [isloading, setIsloading] = useState(false)

    let newUser = {
        name: name,
        email: email,
        password: password,
        role: role,
        avatar: avatar,
    };

    console.log("New User is : ", newUser);

    const handleCreateUser = () => {
        setIsloading(true)
        CREATE_USER(newUser)
            .then((response) => {
                setIsloading(false)
                console.log("Testing", response);
                toast("User Created Successfully!!")
            })
            .catch((error) => {
                console.log("ERORR", error);
                toast(error.response.data.message[0])
                setIsloading(false)
            });
    };

    let btnStatus = name && role && email && password && avatar ? false : true;
    console.log("btn Disable : ", btnStatus)

    const handleClear = () => {
        toast("Button is Clicked")
        setName("")
        setEmail("")
        setPassword("")
        setRole("")
        setAvatar("")
    };

  return (
    <div className="container ">
        <div className="wrapper d-flex bg-primary rounded-3 mt-4  p-4 ">
            <div className="image-side w-50  d-flex justify-content-between flex-column align-items-center">
                <img
                className="object-contain-fit"
                width="300px"
                src="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png"
                alt="user placeholder"
                />
                <input type="file" className="form-control w-50 bg-warning border border-primary " />
            </div>

            <div className="input-side  mt-4  w-50 pe-5 text-black fw-bold">
                <h2 className='text-warning'> User Information </h2>

                <div className="d-flex justify-content- between">
                    <div className='w-50'>
                        <label htmlFor="">Username </label>
                        <input
                        className="form-control"
                        type="text"
                        placeholder="Enter name "
                        name=""
                        id="username"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        />
                    </div>
                    <div className="ms-5 w-50">
                        <label htmlFor="">Role </label>
                        <input
                        className="form-control"
                        type="text"
                        placeholder="admin or customer "
                        name=""
                              id="role"
                              value={role}
                        onChange={(e) => setRole(e.target.value)}
                        />
                    </div>
                </div>

                <div className="d-flex">
                    <div className='w-50'>
                        <label htmlFor="">Email </label>
                        <input
                        type="text"
                        className="form-control"
                              placeholder="Enter email "
                              value={email
                              }
                        onChange={(e) => setEmail(e.target.value)}
                        />
                    </div>

                    <div className="ms-5 w-50">
                        <label htmlFor="">Password </label>
                        <input
                        type="password"
                        className="form-control"
                              placeholder="Enter password "
                              value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                </div>

                  <label htmlFor="">Avatar Links </label>
                  
                <input
                    type="text"
                    className="form-control"
                      placeholder="Enter avatar links "
                      value={avatar}
                    onChange={(e) => setAvatar(e.target.value)}
                
                />

                <div className="d-flex justify-content-between">
                    
                    <button className="btn btn-warning mt-3 fw-bold border border-primary text-center" disabled={btnStatus}
                        onClick={handleCreateUser}>Create User
                    </button>
                    {
                         isloading ? <ThreeDots 
                         height="100" 
                         width="100" 
                         radius="10"
                         color="orange"
                         ariaLabel="three-dots-loading"
                         wrapperStyle={{}}
                         wrapperClassName=""
                         visible={true}
                          /> 
                        :
                        <></>
                    }
                    <button className="btn btn-danger mt-3 ms-5 fw-bold border border-secondary text-center " onClick={handleClear}> Clear</button>
                </div>
                <ToastContainer/>
            </div>
        </div>
    </div>
  )
}
