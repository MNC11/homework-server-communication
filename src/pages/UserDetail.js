import React, { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import { CREATE_USER, GET_ONE, GET_ONE_USER } from "../service/userService";
// import { ThreeDots } from "react-loader-spinner";
import { useParams } from "react-router-dom";
const ViewProfile = () => {
  const [user, setUser] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [avatar, setAvatar] = useState("");
  const { id } = useParams();

  useEffect(() => {
    GET_ONE_USER(id)
      .then((response) => {
        setUser(response);
        console.log("Get one response : ", response);
        setName(response.name);
        setEmail(response.email);
        setRole(response.role);
        setPassword(response.password);
      })
      .catch((err) => console.log("ERROR IS : ", err));
  }, []);

  return (
    <div className="container ">

      <div className="wrapper d-flex bg-warning rounded-3 m-4  p-10 ">

        <div className="image-side w-50  d-flex justify-content-center align-items-center">

          <img
            className="object-contain-fit square border border-primary-4 m-3 rounded-5"
            width="400px"
            src={user.avatar} onError={({ currentTarget }) => {
              currentTarget.onerror = null;
              currentTarget.src = "https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png";
            }}
            alt="user placeholder"
          />

        </div>

        <div className="input-side  mt-4  w-50 pe-5 text-black">

          <h2 className="text-danger fw-bold mb-4">User Detail </h2>

          <div className="d-flex justify-content-center ">

            <div>
              <label htmlFor="">Username </label>
              <input
                className="form-control border border-primary fw-bold"
                type="text"
                id="username"
                value={name}
                disabled
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <div className="ms-4">
              <label htmlFor="">Role </label>
              <input
                className="form-control border border-primary text-danger fw-bold"
                type="text"
                name=""
                id="role"
                value={role}
                disabled
                onChange={(e) => setRole(e.target.value)}
              />
            </div>

          </div>

          <div className="d-flex justify-content-center m-3">

            <div>
              <label htmlFor="">Email </label>
              <input
                type="text"
                className="form-control border border-primary fw-bold"
                disabled
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="ms-4">
              <label htmlFor="">Password </label>
              <input
                type="text"
                className="form-control border border-primary fw-bold"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                disabled
              />
            </div>

          </div>

          <div className="d-flex mt-4 justify-content-center align-item-center p-4">

            <button className="btn btn-info d-flex justify-content-between align-items-center p-1 m-5  px-3 ">

              <svg xmlns="http://www.w3.org/2000/svg" className="mt-1" width="20" height="20" fill="red"  class="bi    bi-pencil-square " viewBox="0 0 16 16">
                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
              </svg>

              <p className="fw-bold p-0 m-2 ">Edit</p>

            </button>

            <button className="btn btn-danger d-flex justify-content-between align-items-center p-1 m-5  px-1">

              <svg xmlns="http://www.w3.org/2000/svg" className="mt-2" width="20" height="20" fill="white" class="bi bi-trash-fill" viewBox="0 0 16 16">
                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
              </svg>

              <p className="fw-bold p-0 m-2">Delete</p>
            </button>

          </div>

          {/* <ToastContainer /> */}

        </div>
        
      </div>
    </div>
  );
};

export default ViewProfile;
