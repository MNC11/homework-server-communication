import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

function UserCard(props) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={props.user.avatar} onError={({ currentTarget }) => {
    currentTarget.onerror = null; // prevents looping
    currentTarget.src="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png";
  }} />
      <Card.Body>
        <Card.Title>{props.user.name} ({props.user.role})</Card.Title>
        <Card.Text>
          {props.user.email}
        </Card.Text>

        <Link to={`${props.user.id}`} >
          <Button variant="primary">View</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}

export default UserCard;